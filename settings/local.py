from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent

# Flask
SECRET_KEY = "4cdfbf631b83a988273950973b16e1fca28d267b"
JSON_SORT_KEYS = True
SECURITY_SALT = "ce48165fdc09715d4809d259c7ef88b7"
SERVER_NAME = "localhost:5000"

# Flask-SQLAlchemy
SQLALCHEMY_DATABASE_URI = f"sqlite:///{BASE_DIR / 'db.sqlite3'}"
SQLALCHEMY_TRACK_MODIFICATIONS = True
SQLALCHEMY_RECORD_QUERIES = True

# Flask-Mail
MAIL_SERVER = "localhost"
MAIL_PORT = 25
MAIL_SENDER = "admin@localhost.dev"

# Flask-WTF
WTF_CSRF_SSL_STRICT = False

# Flask-Login
SESSION_PROTECTION = "strong"
SESSION_COOKIE_HTTPONLY = True

# Cryptography
ENCRYPTION_KEY = b"uiRzXROhCgdsV9CnFn7Mjo3hqqutNAtnRs-O0rZOGs8="

# Huey
HUEY = {
    "name": "giveaway_api",
    "results": False,
    "immediate": True,
    "utc": True,
    "url": "redis://localhost:6379/?db=1",
    "immediate_use_memory": False,
    "consumer": {
        "workers": 4,
        "worker_type": "thread",
    },
}
