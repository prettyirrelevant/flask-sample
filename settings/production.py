from decouple import config

# flake8: noqa
from settings.local import *

SESSION_COOKIE_HTTPONLY = True
SESSION_COOKIE_SECURE = True


SQLALCHEMY_DATABASE_URI = config("DATABASE_URL")
SQLALCHEMY_ECHO = True
