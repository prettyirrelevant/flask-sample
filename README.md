# Getting Started
- Clone the repository
- Create a virtual environment
- Install the project requirements
- Start the development server
```sh
$ flask run
```
- Start the Huey consumer
```sh
$ flask run_huey
```
- Start the smtpd debugging server
```sh
$ python -m smtpd -n -c DebuggingServer localhost:25
```
- Test endpoints using Postman
- Any error you encounter, you be agba!