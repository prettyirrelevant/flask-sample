from os import environ

from flask import Flask

from src.extensions import bcrypt, cors, csrf, db, huey, login_manager, mail, migrate


def create_app():
    app = Flask(__name__)

    # dev config
    app.config.from_object("settings.local")

    # production config
    settings_override = environ.get("FLASK_SETTINGS_MODULE", None)
    app.config.from_object(settings_override)

    bcrypt.init_app(app)
    db.init_app(app)
    csrf.init_app(app)
    huey.init_app(app)
    mail.init_app(app)
    cors.init_app(app)
    login_manager.init_app(app)
    migrate.init_app(app, db)

    with app.app_context():
        from src.blueprints.accounts import accounts
        from src.blueprints.core import core
        from src.blueprints.errors import errors

        app.register_blueprint(accounts, url_prefix="/api/accounts")
        app.register_blueprint(core, url_prefix="/api/core", cli_group=None)
        app.register_blueprint(errors)

    return app
