import base64

from marshmallow import ValidationError, fields, validate, validates, validates_schema
from src.blueprints.accounts.models import User
from src.blueprints.core.utils import CamelCaseSerializer


class UserRegistrationSerializer(CamelCaseSerializer):
    email = fields.Email(required=True)
    full_name = fields.Str(required=True, validate=validate.Length(min=6, max=80))
    password = fields.Str(required=True, validate=validate.Length(min=8, max=36))

    @validates("email")
    def validate_email(self, data):
        user = User.query.filter_by(email=data).first()
        if user:
            raise ValidationError("Email address already in use!")


class UserLoginSerializer(CamelCaseSerializer):
    email = fields.Email(required=True)
    password = fields.Str(required=True, validate=validate.Length(min=8, max=36))


# no validation because it is just used to dump
class UserProfileSerializer(CamelCaseSerializer):
    email = fields.Email()
    full_name = fields.Str()
    profile_url = fields.Method("get_profile_url")
    is_admin = fields.Bool()

    def get_profile_url(self, obj):
        if isinstance(obj, dict):
            encoded_data = obj["full_name"].encode()
        elif isinstance(obj, User):
            encoded_data = obj.full_name.encode()

        b64_encoded_data = base64.b64encode(encoded_data).decode("utf-8")
        return f"https://avatars.dicebear.com/api/bottts/{b64_encoded_data}.svg"


class ResetPasswordRequestSerializer(CamelCaseSerializer):
    email = fields.Str(required=True, validate=validate.Length(equal=11))


class ResetPasswordSerializer(CamelCaseSerializer):
    new_password = fields.Str(required=True, validate=validate.Length(min=8, max=36))
    confirm_new_password = fields.Str(required=True, validate=validate.Length(min=8, max=36))

    @validates_schema
    def validate_passwords(self, data, **kwargs):
        errors = {}
        if data["new_password"] != data["confirm_new_password"]:
            errors["confirm_new_password"] = "Passwords do not match!"

        if errors:
            raise ValidationError(errors)
