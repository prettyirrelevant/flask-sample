import logging

from src.app import create_app
from src.extensions import huey

from .utils import send_activation_email, send_password_reset_mail

logger = logging.getLogger("huey")


@huey.task()
def send_async_activation_email(recipient, name):
    app = create_app()

    logger.info(f"Sending activation email to -> Email:{recipient} Full Name:{name}")

    with app.app_context():
        send_activation_email(recipient, name)

    logger.info(f"Activation email sent successfully to -> Email:{recipient} Full Name:{name}")


@huey.task()
def send_async_password_reset_mail(recipient, name):
    app = create_app()

    logger.info(f"Sending password reset email to -> Email:{recipient} Full Name:{name}")

    with app.app_context():
        send_password_reset_mail(recipient, name)

    logger.info(f"Password reset email sent successfully to -> Email:{recipient} Full Name:{name}")
