import datetime

from flask import jsonify, request
from flask_login import current_user, login_required, login_user, logout_user
from flask_wtf.csrf import generate_csrf
from marshmallow.exceptions import ValidationError
from src.extensions import db

from . import accounts
from .models import User
from .serializers import (
    ResetPasswordRequestSerializer,
    ResetPasswordSerializer,
    UserLoginSerializer,
    UserProfileSerializer,
    UserRegistrationSerializer,
)
from .tasks import send_async_activation_email, send_async_password_reset_mail
from .utils import verify_activation_token, verify_password_reset_token


@accounts.post("/registration")
def registration():
    request_data = request.get_json()
    serializer = UserRegistrationSerializer()

    try:
        data = serializer.load(request_data)
    except ValidationError as error:
        return jsonify(status="error", detail=error.messages), 400

    new_user = User(**data)

    db.session.add(new_user)

    db.session.commit()

    send_async_activation_email(new_user.email_address, new_user.full_name)

    return (
        jsonify(
            status="success",
            detail="Account created successfully. Please check email to activate account!",
        ),
        201,
    )


@accounts.post("/login")
def login():
    request_data = request.get_json()
    serializer = UserLoginSerializer()

    try:
        data = serializer.load(request_data)
    except ValidationError as error:
        return jsonify(status="error", detail=error.messages), 400

    email, password = data.get("email"), data.get("password")

    user: User = User.query.filter_by(email=email).first()

    if not user or not user.compare_pw(password):
        return jsonify(status="error", detail="Invalid credentials!"), 400

    login_user(user)

    return jsonify(status="success", detail="User logged in successfully!"), 200


@accounts.get("/activate_account/<token>")
def activate_account(token):
    user: User = verify_activation_token(token)

    if not user:
        return (
            jsonify(status="error", detail="The confirmation link is invalid or has expired."),
            400,
        )

    if user.is_verified:
        return jsonify(status="success", detail="Account already confirmed!"), 200

    user.is_verified = True
    user.email_verified_at = datetime.datetime.utcnow()

    db.session.commit()

    return jsonify(status="success", detail="You have successfully activated your account!"), 200


@accounts.get("/resend_activation_mail")
@login_required
def resend_activation_mail():
    send_async_activation_email(current_user.email_address, current_user.full_name)

    return jsonify(status="success", detail="A new activation email has been sent!"), 200


@accounts.post("/reset_password_request")
def reset_password_request():
    request_data = request.get_json()
    serializer = ResetPasswordRequestSerializer()

    try:
        data = serializer.load(request_data)
    except ValidationError as error:
        return jsonify(status="error", detail=error.messages), 400

    email = data.get("email")

    user: User = User.query.filter_by(email=email).first()

    if user:
        send_async_password_reset_mail(user.phone_number, user.full_name)

    return (
        jsonify(
            status="success",
            detail="Check your email for the instructions to reset your password!",
        ),
        200,
    )


@accounts.post("/reset_password/<token>")
def reset_password(token):
    user: User = verify_password_reset_token(token)
    if not user:
        return (
            jsonify(status="error", detail="The password reset token is invalid or has expired!"),
            400,
        )

    request_data = request.get_json()
    serializer = ResetPasswordSerializer()

    try:
        data = serializer.load(request_data)
    except ValidationError as error:
        return jsonify(status="error", detail=error.messages), 400

    # delete current user session
    logout_user()

    user.set_password(data.get("new_password"))
    db.session.commit()

    return jsonify(status="success", detail="Password reset successful!"), 200


@accounts.delete("/logout")
def logout():
    logout_user()
    return jsonify(status="success", detail="User logged out successfully!"), 200


@accounts.get("/me")
@login_required
def me():
    serializer = UserProfileSerializer()
    data = serializer.dump(current_user)
    return jsonify(data), 200


@accounts.get("/get_csrf")
def get_csrf():
    token = generate_csrf()
    response = jsonify(status="success", message="CSRF cookie set!")
    response.headers.set("X-CSRFToken", token)
    return response
