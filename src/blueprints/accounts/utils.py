from flask import current_app, render_template, url_for
from flask_mail import Message
from itsdangerous import URLSafeTimedSerializer
from src.extensions import mail

from .models import User


def send_activation_email(recipient, name):
    token = generate_activation_token(recipient)
    activation_url = url_for("accounts.activate_account", token=token)

    subject = "[Giveaway.africa] Please confirm your email!"
    sender = current_app.config.get("MAIL_SENDER")

    html_body = render_template(
        "accounts/activate_account.html", activation_url=activation_url, name=name
    )

    message = Message(subject, recipients=[recipient], html=html_body, sender=sender)

    mail.send(message)


def send_password_reset_mail(recipient, name):
    token = generate_password_reset_token(recipient)
    reset_password_url = url_for("accounts.reset_password", token=token)

    subject = "[Giveaway.africa] Reset Your Password"
    sender = current_app.config.get("MAIL_SENDER")

    html_body = render_template(
        "accounts/reset_password.html", reset_password_url=reset_password_url, name=name
    )

    message = Message(subject, recipients=[recipient], html=html_body, sender=sender)

    mail.send(message)


def generate_activation_token(email):
    serializer = URLSafeTimedSerializer(
        current_app.config.get("SECRET_KEY"), salt=current_app.config.get("SECURITY_SALT")
    )
    return serializer.dumps(email)


def generate_password_reset_token(email):
    serializer = URLSafeTimedSerializer(
        current_app.config.get("SECRET_KEY"), salt=current_app.config.get("SECURITY_SALT")
    )

    return serializer.dumps(email)


def verify_password_reset_token(token, expires_in=300):
    serializer = URLSafeTimedSerializer(
        current_app.config.get("SECRET_KEY"), salt=current_app.config.get("SECURITY_SALT")
    )
    try:
        email = serializer.loads(token, max_age=expires_in)
    # flake8: noqa
    except:
        return
    return User.query.filter_by(email_address=email).first()


def verify_activation_token(token, expires_in=1800):
    serializer = URLSafeTimedSerializer(
        current_app.config.get("SECRET_KEY"), salt=current_app.config.get("SECURITY_SALT")
    )
    try:
        email = serializer.loads(token, max_age=expires_in)

    # flake8: noqa
    except:
        return False

    return User.query.filter_by(email_address=email).first()
