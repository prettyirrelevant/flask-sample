from flask_login import UserMixin
from sqlalchemy import func
from src.extensions import bcrypt, db, login_manager


class User(UserMixin, db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)

    email = db.Column(db.String(255), nullable=False, unique=True, index=True)

    password_hash = db.Column(db.String(255), nullable=False, unique=True)

    full_name = db.Column(db.String(80), nullable=False)

    is_verified = db.Column(db.Boolean, default=False)

    is_admin = db.Column(db.Boolean, default=False)

    email_verified_at = db.Column(db.DateTime, nullable=True)

    created_at = db.Column(db.DateTime, server_default=func.now())

    cylinders = db.relationship("Cylinder", backref="user", lazy="dynamic", passive_deletes=True)

    def __init__(self, email, password, full_name):
        self.email = email
        self.password_hash = bcrypt.generate_password_hash(password).decode("utf-8")
        self.full_name = full_name

    def compare_pw(self, guess):
        return bcrypt.check_password_hash(self.password_hash, guess)

    def set_password(self, new_password):
        self.password_hash = bcrypt.generate_password_hash(new_password)

    @property
    def password(self):
        raise NotImplementedError("Sorry, you cannot access password!")

    def __repr__(self) -> str:
        return f"<User email={self.email_address}>"


@login_manager.user_loader
def load_user(id: int):
    return User.query.get(id)
