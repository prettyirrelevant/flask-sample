from flask import Blueprint

accounts = Blueprint("accounts", __name__)

# flake8: noqa
from src.blueprints.accounts import views
