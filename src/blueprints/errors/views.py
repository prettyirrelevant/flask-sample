from flask import jsonify

from . import errors


@errors.app_errorhandler(500)
def internal_error(e):
    return jsonify(status="error", detail=e.description), 500


@errors.app_errorhandler(400)
def bad_request(e):
    return jsonify(status="error", detail=e.description), 400


@errors.app_errorhandler(404)
def not_found(e):
    return jsonify(status="error", detail=e.description), 404


@errors.app_errorhandler(405)
def method_not_allowed(e):
    return jsonify(status="error", detail=e.description), 405


@errors.app_errorhandler(401)
def unauthorized(e):
    return jsonify(status="error", detail=e.description), 401


@errors.app_errorhandler(403)
def forbidden(e):
    return jsonify(status="error", detail=e.description), 403
