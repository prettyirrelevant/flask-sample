from flask import Blueprint

errors = Blueprint("errors", __name__)

# flake8: noqa
from src.blueprints.errors import views
