from flask import current_app, jsonify
from flask_sqlalchemy import get_debug_queries

from . import core
from .utils import get_db_status


@core.get("/health")
def get_health():
    status = {}

    db_status = get_db_status()
    status.update(db_status)

    return jsonify(statuses=status), 200


# @core.after_app_request
# def after_request(response):
#     for query in get_debug_queries():
#         current_app.logger.debug(
#             "Query: %s\nParameters: %s\nDuration: %fs\nContext: %s\n"
#             % (query.statement, query.parameters, query.duration, query.context)
#         )
#     return response
