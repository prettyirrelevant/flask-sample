import re
import unicodedata
from importlib import import_module

from flask import current_app
from marshmallow import Schema
from sqlalchemy.sql import text
from src.extensions import db


def get_db_status():
    try:
        db.session.query(text("1")).from_statement(text("SELECT 1")).all()
        return {"db": "Working fine!"}
    # flake8: noqa
    except:
        return {"db": "Something is wrong!"}


def slugify(value):
    value = str(value)
    value = unicodedata.normalize("NFKD", value).encode("ascii", "ignore").decode("ascii")
    value = re.sub(r"[^\w\s-]", "", value.lower())
    return re.sub(r"[-\s]+", "-", value).strip("-_")


def camelcase(s):
    parts = iter(s.split("_"))
    return next(parts) + "".join(i.title() for i in parts)


def autodiscover_tasks():
    for key, value in current_app.blueprints.items():
        try:
            import_module(".tasks", value.import_name)

        except ModuleNotFoundError as err:
            pass

    # try importing from project root
    root_path = ".".join(current_app.import_name.split(".")[:-1])
    try:
        import_module(".tasks", root_path)
    except ModuleNotFoundError as error:
        pass


class CamelCaseSerializer(Schema):
    """Schema that uses camel-case for its external representation
    and snake-case for its internal representation.
    """

    def on_bind_field(self, field_name, field_obj):
        field_obj.data_key = camelcase(field_obj.data_key or field_name)
