from flask import Blueprint

core = Blueprint("core", __name__)

# flake8: noqa
from src.blueprints.core import cli, views
