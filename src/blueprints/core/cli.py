import logging

from flask import current_app
from huey.consumer_options import ConsumerConfig

from . import core
from .utils import autodiscover_tasks


@core.cli.command("run_huey")
def run_huey():
    """Command to run huey consumer"""
    HUEY = current_app.extensions["huey"]

    consumer_options = {}

    try:
        if isinstance(current_app.config["HUEY"], dict):
            consumer_options.update(current_app.config["HUEY"].get("consumer", {}))
    except AttributeError:
        pass

    consumer_options.setdefault("verbose", consumer_options.pop("huey_verbose", None))

    # autodiscover every `tasks.py` module in every blueprints
    autodiscover_tasks()

    logger = logging.getLogger("huey")

    config = ConsumerConfig(**consumer_options)
    config.validate()

    if not logger.handlers:
        config.setup_logger(logger)

    consumer = HUEY.create_consumer(**config.values)
    consumer.run()
