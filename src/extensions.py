from flask_bcrypt import Bcrypt
from flask_cors import CORS
from flask_login import LoginManager
from flask_mail import Mail
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import CSRFProtect
from huey import RedisHuey


class Huey:
    __app = None
    __instance = None

    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self.__app = app

        _config = app.config.get("HUEY")

        if not _config:
            raise Exception("Huey requires a configuration in `app.config`")

        if not isinstance(_config, dict):
            raise Exception("Huey expects a dictionary as its configuration")

        _huey_instance = self.init_huey(_config)

        app.extensions["huey"] = _huey_instance
        self.__instance = _huey_instance

    def init_huey(self, config):
        _huey = RedisHuey(
            name=config.get("name", self.__app.name),
            url=config.get("url", None),
            store_none=config.get("store_none", False),
            blocking=config.get("blocking", False),
            read_timeout=config.get("read_timeout", 1),
            utc=config.get("utc", True),
        )
        return _huey

    def __getattr__(self, name):
        return getattr(self.__instance, name, None)


bcrypt = Bcrypt()
cors = CORS()
csrf = CSRFProtect()
db = SQLAlchemy()
login_manager = LoginManager()
mail = Mail()
migrate = Migrate()
huey = Huey()
